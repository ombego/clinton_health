<div class="page-title">
    <h2><span class="fa fa-arrow-circle-o-left"></span> <?php echo $page_title; ?></h2>
</div>

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Drug Orders</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Facility</th>
                            <th>Period</th>
                            <th>Code</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $num=0; foreach($orders as $row): $num++; ?>
                        <tr>
                            <td><?php echo $num; ?></td>
                            <td><?php echo ($row->facility_name)?$row->facility_name:ucwords (strtolower($row->county_name))." County";  ?></td>
                            <td><?php e(date('d-M-Y',strtotime($row->order_date))); ?></td>
                            <td><?php echo($row->category)?$row->category:"D-CDRR"; ?></td>
                            <td><a href="#txtResult" data-toggle="modal" title="View Order Items" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url(); ?>home/order_details', 'ch=<?php echo $row->id; ?>')">View</td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>

</div>
<div id="txtResult" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true"> </div>
<!-- END PAGE CONTENT WRAPPER -->