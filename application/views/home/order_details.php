<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
//$value = $_GET['ch'];

// List users on the list box
$url=base_url();

?>

			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
						<h3>Order Items</h3>
					</div>
					<div class="modal-body">
						<table class="table table-hover">
							<thead>
							<tr>
								<th>#</th>
								<th>Product</th>
								<th>Start Balance</th>
								<th>Close Balance</th>
								
							</tr>
							</thead>
							<tbody>
							<?php $num=0; foreach($order_items as $row): $num++; ?>
							<tr>
								<td><?php echo $num; ?></td>
								<td><?php e($row->name); ?></td>
								<td><?php e($row->order_qty); ?></td>
								<td><?php e($row->current_qty); ?></td>                            
							</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">						
						<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
					</div>
				</div>
			</div>
					
				