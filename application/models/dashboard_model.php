<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends MY_Model {
	protected $table_name = 'orders';
    protected $key = 'id';
    protected $set_created = true;
    protected $log_user = false;
    protected $set_modified = false;
    protected $soft_deletes = false;
    protected $date_format = 'datetime';
	
	protected $created_field    = 'create_on';
    
   
	public function get_orders()
	{
		return $this->db->query("SELECT bf_orders.*,county_name,facility_name,category,display_name FROM bf_orders 
									LEFT JOIN bf_users On bf_users.id=bf_orders.order_by
									LEFT JOIN bf_facilities ON bf_facilities.id=bf_users.allocation
									LEFT JOIN bf_counties ON bf_counties.county_id=bf_users.allocation")->result();
	}
	public function get_facility_orders($facility_id)
	{
		return $this->db->query("SELECT bf_orders.*,county_name,facility_name,category,display_name FROM bf_orders 
									LEFT JOIN bf_users On bf_users.id=bf_orders.order_by
									LEFT JOIN bf_facilities ON bf_facilities.id=bf_users.allocation
									LEFT JOIN bf_counties ON bf_counties.county_id=bf_users.allocation
									WHERE bf_facilities.id='".$facility_id."'")->result();
	}
	public function get_county_orders($county_id)
	{
		return $this->db->query("SELECT bf_orders.*,county_name,facility_name,category,display_name FROM bf_orders 
									LEFT JOIN bf_users On bf_users.id=bf_orders.order_by
									LEFT JOIN bf_facilities ON bf_facilities.id=bf_users.allocation
									LEFT JOIN bf_counties ON bf_counties.county_id=bf_users.allocation
									WHERE bf_counties.county_id='".$county_id."'")->result();
	}
	public function get_order_items($order_id)
	{
		return $this->db->query("SELECT bf_order_items.*,name FROM bf_order_items
									LEFT JOIN bf_inventory ON bf_inventory.id=product_id
									WHERE order_id='".$order_id."'")->result();
	}
}