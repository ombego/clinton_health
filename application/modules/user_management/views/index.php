<div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> <?php echo $page_title; ?></h2>
                </div>                   
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Manage Users</h3>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target=".new_user"><i class="glyphicon glyphicon-user"></i> Create New user</button>
									<div role="tabpanel" data-example-id="togglable-tabs">
                                        <ul id="myTab1" class="nav nav-tabs" role="tablist">
											<li role="presentation" class="active"><a href="#tab_content22" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false">Active Users</a>
                                            </li>
                                            <li role="presentation" class=""><a href="#tab_content11" id="home-tabb" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Inactive Users</a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent2" class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade" id="tab_content11" aria-labelledby="home-tab">
                                                <p><table class="table datatable table-hover">
														<thead>
															<tr>
																<th>#</th>
																<th>User Name</th>
																<th>Full Names</th>
																<th>Email</th>
																<th>Role</th>
																<th>Allocation</th>
																<th>Last Login</th>
																<th>Actions</th>
															</tr>
														</thead>
														<tbody>
															
															<?php $num=0; foreach($users as $row): if($row->banned==1): $num++;?>
															<tr>
																<th scope="row"><?php echo $num; ?></th>
																<td><?php echo $row->username; ?></td>
																<td><?php echo $row->display_name; ?></td>
																<td><a href="mailto:<?php echo $row->email; ?>"><?php echo $row->email; ?></a></td>
																<td><?php echo $row->role_name; ?></td>
																<td><?php if($row->role_id==8){echo $row->county_name.' County';}elseif($row->role_id==9){echo $row->facility_name.' Facility';}else{echo 'National';} ?></td>
																<td><?php echo $row->last_login; ?></td>
																<td>
																		<?php if($row->banned==0): ?>
																		<a href="#txtResult" data-toggle="modal" title="Deactivate User" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>user_management/disable_user', 'ch2=<?php echo $row->id?>&ch=disable')"><i class="fa fa-lock"></i></a>
																		<?php else: ?>
																		<a href="#txtResult" data-toggle="modal" title="Activate User" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>user_management/disable_user', 'ch2=<?php echo $row->id?>&ch=enable')"><i class="fa fa-unlock"></i></a>
																		<?php endif;?>
																	
																</td>
															</tr>
															
															<?php endif; endforeach; ?>															
														</tbody>
													</table>
												</p>
                                            
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content22" aria-labelledby="profile-tab">
                                                <p><table class="table datatable table-hover">
														<thead>
															<tr>
																<th>#</th>
																<th>User Name</th>
																<th>Full Names</th>
																<th>Email</th>
																<th>Role</th>
                                                                <th>Allocation</th>
																<th>Last Login</th>
																<th>Actions</th>
															</tr>
														</thead>
														<tbody>
															
															<?php $num=0; foreach($users as $row): if($row->banned==0): $num++;?>
															<tr>
																<th scope="row"><?php echo $num; ?></th>
																<td><?php echo $row->username; ?></td>
																<td><?php echo $row->display_name; ?></td>
																<td><a href="mailto:<?php echo $row->email; ?>"><?php echo $row->email; ?></a></td>
																<td><?php echo $row->role_name; ?></td>
                                                                <td><?php if($row->role_id==8){echo $row->county_name.' County';}elseif($row->role_id==9){echo $row->facility_name.' Facility';}else{echo 'National';} ?></td>
																<td><?php echo $row->last_login; ?></td>
																<td>
																		<a href="#txtResult" data-toggle="modal"  title="Change Password" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>user_management/change_password', 'ch2=<?php echo $row->id?>&ch=Maintenace Order')"><i class="fa fa-shield"></i></a>
																		<a href="#txtResult" data-toggle="modal" title="Edit User info" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>user_management/edit_user', 'ch2=<?php echo $row->id?>&ch=Maintenace Order')"><i class="fa fa-edit"></i></a>
																		<?php if($row->banned==0): ?>
																		<a href="#txtResult" data-toggle="modal" title="Deactivate User" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>user_management/disable_user', 'ch2=<?php echo $row->id?>&ch=disable')"><i class="fa fa-lock"></i></a>
																		<?php else: ?>
																		<a href="#txtResult" data-toggle="modal" title="Activate User" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>user_management/disable_user', 'ch2=<?php echo $row->id?>&ch=enable')"><i class="fa fa-unlock"></i></a>
																		<?php endif;?>
																</td>
															</tr>
															
															<?php endif; endforeach; ?>															
														</tbody>
													</table>
												</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER --> 
				<div id="txtResult" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true"> </div>
				<div id="txtResult2" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true"> </div>

                  <!-- Modal for new user --> 
							
								<div class="modal fade new_user" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
										<form method="post" id="wizard" class="form-horizontal" role="form" action="<?php echo base_url(); ?>user_management/new_user" accept-charset="utf-8" autocomplete="off">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">New User Account</h4>
                                            </div>
                                            <div class="modal-body">
                                                
                                                
											<div class="wizard-steps clearfix"></div>
											
												<div class="form-group">
													<label class="col-lg-4 control-label" for="username">User Name:</label>
													<div class="col-lg-5">
														<input id="username" name="username" required="required" type="text" class="form-control col-md-7 col-xs-12" />
													</div>
												</div><!-- End .form-group  -->
												<div class="form-group">
													<label class="col-lg-4 control-label" for="username">Full Names:</label>
													<div class="col-lg-5">
														<input id="display_name" required="required" name="display_name" type="text" class="form-control col-md-7 col-xs-12" />
													</div>
												</div><!-- End .form-group  -->
												
											
												<div class="form-group">
													<label class="col-lg-4 control-label" for="email">email:</label>
													<div class="col-lg-5">
														<input class="form-control" required="required" id="email" name="email" type="email" />
													</div>
												</div><!-- End .form-group  -->
												<div class="form-group">
													<label class="col-lg-4 control-label" for="phone">Phone:</label>
													<div class="col-lg-5">
														<input class="form-control" required="required" id="phone" name="phone" type="number" />
														<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
																<!--Since we have skiped some enrollment bit we hide some of the fields and send registration to the method with default values 
																<input hidden class="span6" id="pass_confirm" name="pass_confirm" value="default">-->
																<input hidden name="timezones" value="UP3" >
																<input hidden name="language" value="English" >
																<input hidden name="street_name" value="Kenya" >
																<input hidden name="state" value="KE" >
																<input hidden name="country" value="KE" >
																<input hidden name="language" value="English" >
																
													</div>
												</div><!-- End .form-group  -->
                                                <div class="form-group">
													<label class="col-lg-4 control-label" for="role">User Level:</label>
													<div class="col-lg-5">
														<select name="role" required="required" id="role" class="form-control" onChange="htmlData3('<?php echo base_url();?>user_management/filter_allocation', 'ch='+this.value+'&ch2=2')">
															<option selected="selected" value="" disabled>Select User level</option>
															<?php foreach($roles as $role):?>
															<option value="<?php echo $role->role_id; ?>"><?php echo $role->role_name; ?></option>
															<?php endforeach; ?>
														</select>
													</div>
                                                </div><!-- End .form-group  -->
                                                <div class="form-group" id="txtResult3">

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <input type="submit" name="register"  class="btn btn-primary" value="Register New User">
                                            </div>
										</form>
                                        </div>
                                    </div>
                                </div>

<!-- Modal for departments -->

<div class="modal fade departments_modal" id="departments_modal"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Departments</h4>
                </div>
                <div class="modal-body">
                    <form method="post" id="departments_form" name="departments_form" class="form-horizontal" role="form" action="<?php echo base_url(); ?>user_management/departments" accept-charset="utf-8" autocomplete="off">
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Department</label>
                            <div class="col-md-6">
                                <input id="department" name="department" required="required" type="text" class="form-control" />
                            </div>
                            <div class="col-md-3">
                                <input type="submit" name="submit"  class="btn btn-warning" value="Create Department">
                            </div>

                        </div>

                    </form>
                   <br> <span class="h4">Department</span>
                    <hr>

                   <table class="table">
                       <thead>
                           <tr>
                               <th>Department</th>
                               <th>Status</th>
                           </tr>
                       </thead>
                       <tbody>
                       <?php if($departments): foreach($departments as $r): ?>
                            <tr>
                                <td><?php echo $r->department; ?></td>
                                <td><?php echo ($r->status==1)?"Active":"<span class='text-danger'>Deleted</span>"; ?></td>
                            </tr>
                       <?php endforeach; endif; ?>
                       </tbody>
                       </tbody>
                   </table>
                </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

        </div>
    </div>
</div>