<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_management extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		Assets::add_module_js('user_management', 'jquery.dataTables.min');
				
		$this->load->model('user_accounts_model');
        $this->load->model('facilities_model');
        $this->load->model('locations_model');

	}
	
	public function index()
	{
		$this->auth->restrict('Vision.User_accounts.View');
		$user = $this->current_user->id;
        Template::set('counties', $this->locations_model->get_counties());
        Template::set('sub_counties', $this->locations_model->get_sub_counties());
        Template::set('facilities', $this->facilities_model->join("bf_constituencies", "bf_constituencies.id=bf_facilities.sub_county", "left")
                        ->select("bf_facilities.*,bf_constituencies.constituency")
                        ->where(array('status'=>1))
                        ->find_all());
		Template::set('users', $this->user_accounts_model->get_users_details());
		Template::set('roles', $this->user_accounts_model->get_user_roles());
		Template::set_theme('default');
		Template::set('page_title', 'User Management');
		Template::render('');
	}

	public function new_user()
		{
			$this->auth->restrict('Vision.User_accounts.Manage');
			if($this->input->post("register")){
				$username=$this->input->post("username");
				$fullnames=$this->input->post("display_name");
				$email=$this->input->post("email");
				$phone=$this->input->post("phone");
				
				if($this->input->post("role")){
					$role = $this->input->post("role");
				}else{
					$role=8;
				}	
				//check if username has been used
				$username_check = $this->user_accounts_model->find_by("username",$username);
				$email_check = $this->user_accounts_model->find_by("email",$email);
				if($this->user_accounts_model->find_by("username",$username)){
					Template::set_message('The user was not succesfully created. The username is already in use.', 'alert fresh-color alert-danger');
				}elseif($this->user_accounts_model->find_by("email",$email)){
					Template::set_message('The user was not succesfully created. The email is already in use.', 'alert fresh-color alert-danger');
				}else{
					//generate a random pasword for the user
					$random_pass= mt_rand(100000,999999);
					$password= $this->auth->hash_password($random_pass);
					if(ISSET($_POST['allocation'])){$allocation=$_POST['allocation'];}else{$allocation=0;}
					$data = array(
						'username'=> $this->input->post('username'),
						'password_hash'=> $password['hash'],
						'display_name'=> $this->input->post('display_name'),
						'allocation'=> $allocation,
						'email'=> $this->input->post('email'),
						'phone'=> $this->input->post('phone'),
						'role_id'=> $role,
						'language'		=> $this->input->post('language'),
						'timezone'		=> $this->input->post('timezones'),

						);
					if($this->user_accounts_model->insert($data)){
						// Log the Activity
						log_activity($this->auth->user_id(),"Created new user: ".$this->input->post('display_name'), 'system_utilities');
						Template::set_message('The user account for <b>'.$this->input->post('username').'</b> was succesfully created. An email has been sent to <b>'.$this->input->post('email').'</b> with account details and password.', 'alert fresh-color alert-success');
					}else{
						Template::set_message('Error Saving!! The was a problem creating the new user. Please check the details submitted.', 'alert fresh-color alert-danger');
					}
					
					//send the user an email
					header('Content-type: application/json');
					//$user = $this->notifications_model->get_transport_order_detail($orderId);
					$name = @trim(stripslashes("Ordering System")); 
					$email = @trim(stripslashes("Account Notification")); 
					$subject = @trim(stripslashes("Account Succesfully Created")); 
					$message = @trim(stripslashes("A user account has been created for you at the Ordering System with the following credentials.\n\n Username: ".$username." \n Password: ".$random_pass." \n You can now be able to order for services with your account and you will receive notifications on your order status through this email.\n For clarifications or assitance please reply to this email." ));
					$email_from = 'ombego@gmail.com';
					$email_to = $this->input->post('email');
					$body = 'Name: ' . $name . "\n\n" . 'Email: ' . $email . "\n\n" . 'Subject: ' . $subject . "\n\n" . 'Message: ' . $message;
					$success = @mail($email_to, $subject, $body, 'From: <'.$email_from.'>');
					redirect("user_management/index");
				}
			}
			
			//$this->auth->restrict('Vision.system_utilities.User_Manage');
			Template::set_theme('default');
			Template::set('page_title', 'New User ');
			Template::render('');
		}
	
	public function change_password()
        {
			
			if($this->input->post("submit")){
				$id=$this->input->post("userId");
				$password = $this->input->post("password");
				$repeat_password = $this->input->post("repeat_password");
				if($password == $repeat_password){
					$password= $this->auth->hash_password($password);
					$data['password_hash']=$password['hash'];
					if ($this->user_accounts_model->update($id, $data))
					{
						// Log the Activity
						log_activity($this->auth->user_id(),"Changed password for: User id ".$id, 'system_utilities');
						Template::set_message('The user password was successfully changed.', 'alert fresh-color alert-success');
						redirect('user_management/index',true);
					}
				}else{
					Template::set_message('Sorry the password was not changed.The passwords submitted did not match.', 'alert fresh-color alert-danger');
					redirect('user_management/index',true);
				}
			}else{
				$id = $this->input->get("ch2");			
				$details = $this->user_accounts_model->as_object()->find($id);
				$security_name = $this->security->get_csrf_token_name();
					$security_code = $this->security->get_csrf_hash();
					$url = base_url()."user_management/change_password";
					echo <<<eod
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
								<h3>Change Password</h3>
							</div>
							<div class="modal-body">
								<h4 align="center">Change the password for user <u> $details->display_name </u></h4>
								<form class="form-horizontal" method="post" action="$url" role="form">
								<div class="row">
									<div class="form-group">
										<label class="col-lg-5 control-label" for="username">New Password:</label>
											<div class="col-lg-3">
												<input id="password" name="password" required="required" type="password" data-validate-length="6,8" class="form-control" />							
												<input type="hidden" name="userId" value="$id">								
												<input type="hidden" name="$security_name" value="$security_code" > 
											</div>
									</div>
									<div class="form-group">
										<label class="col-lg-5 control-label" for="username">Confirm Password:</label>
											<div class="col-lg-3">
												<input id="repeat_password" name="repeat_password" data-validate-linked="password" required="required" type="password"  class="form-control" />							
											
											</div>
									</div>
								
								<div class="modal-footer">						
									<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-vcard white"></span> Change Password</button>
									<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
								</div>
								</form>
							</div>
						</div>
					</div>
eod;
			}
		}
	public function edit_password()
        {
			
			if($this->input->post("submit")){
				$id=$this->input->post("userId");
				$password = $this->input->post("password");
				$repeat_password = $this->input->post("repeat_password");
				if($password == $repeat_password){
					$password= $this->auth->hash_password($password);
					$data['password_hash']=$password['hash'];
					if ($this->user_accounts_model->update($id, $data))
					{
						Template::set_message('The user password was successfully changed.', 'success');
						redirect('',true);
					}
				}else{
					Template::set_message('Sorry the password was not changed.The passwords submitted did not match.', 'error');
					redirect('',true);
				}
			}else{
				$id = $this->input->get("ch2");			
				$details = $this->user_accounts_model->as_object()->find($id);
				$security_name = $this->security->get_csrf_token_name();
					$security_code = $this->security->get_csrf_hash();
					$url = base_url()."user_management/edit_password";
					echo <<<eod
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
								<h3>Change Your Password</h3>
							</div>
							<div class="modal-body">
							<h4 align="center">Change the password for  <u> $details->display_name </u></h4>
							<form class="form-horizontal" method="post" action="$url" role="form">
								<div class="row">
									<div class="form-group">
										<label class="col-lg-5 control-label" for="username">New Password:</label>
											<div class="col-lg-3">
												<input id="password" name="password" required="required" type="password"  class="form-control" />							
											<input type="hidden" name="userId" value="$id">								
											<input type="hidden" name="$security_name" value="$security_code" > 
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-5 control-label" for="username">Confirm Password:</label>
											<div class="col-lg-3">
												<input id="repeat_password" name="repeat_password" required="required" type="password"  class="form-control" />							
											
										</div>
									</div>
								
								<div class="modal-footer">						
									<button type="submit" name="submit" value="submit" class="btn btn-danger"> <span class="icon16 icomoon-icon-vcard white"></span> Change Password</button>
									<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
								</div>
								</form>
							</div>
						</div>
					</div>
eod;
			}
		}
	public function edit_user()
        {
			
			if($this->input->post("submit")){
				$id=$this->input->post("userId");
				$data = array(
					'display_name'=> $this->input->post('fullnames'),
					'email'=> $this->input->post('email'),
                    'role_id'=> $this->input->post('role'),
					'phone'=> $this->input->post('phone')
					);
				if ($this->user_accounts_model->update($id, $data))
				{
					// Log the Activity
					log_activity($this->auth->user_id(),"Changed password user details for: User id ".$this->input->post('fullnames'), 'system_utilities');
					Template::set_message('The user details were successfully edited.', 'alert fresh-color alert-success');
					redirect('user_management/index',true);
				}else{
					Template::set_message('Error Saving!! A problem was encountered editing user details. Please check the values submitted.', 'alert fresh-color alert-danger');
					redirect('user_management/index',true);
				}
					
			}else{
			
			$id = $this->input->get("ch2");
			$details = $this->user_accounts_model->get_user_details($id);
            $roles = $this->user_accounts_model->get_user_roles();
			$security_name = $this->security->get_csrf_token_name();
				$security_code = $this->security->get_csrf_hash();
				$url = base_url()."user_management/edit_user";
				echo <<<eod
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
						<h3>Edit User Infomation</h3>
					</div>
					<div class="modal-body">
					
					<form class="form-horizontal" method="post" action="$url" role="form">
						<div class="row">
							<div class="form-group">
								<label class="col-lg-5 control-label" for="fullnames">Full Names</label>
									<div class="col-lg-3">
										<input id="fullnames" name="fullnames" required="required" type="text" value="$details->display_name" class="form-control" />							
									<input type="hidden" name="userId" value="$id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="email">email</label>
									<div class="col-lg-3">
										<input id="email" name="email" required="required" type="email" value="$details->email" class="form-control" />							
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="phone">Phone Contact</label>
									<div class="col-lg-3">
										<input id="phone" name="phone" required="required" type="number" value="$details->phone" class="form-control" />									
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-lg-5 control-label" for="phone">User Level</label>
									<div class="col-lg-3">
										<select id="role" name="role" required="required"  class="form-control" />
											<option value="">Select User Level</option>	
eod;
                                            foreach($roles as $role){
                                                if($role->role_id==$details->role_id){$s="selected";}else{$s="";};
                                                echo "<option ".$s." value=\"".$role->role_id."\">".$role->role_name."</option>";
                                            }
				echo <<<eod
										</select>
								    </div>
							</div>						
						<div class="modal-footer">						
							<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-pencil-3 white"></span> Save Changes</button>
							<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						</div>
						</form>
					</div>
				</div>
eod;
			}
		}
	public function edit_profile()
        {
			if($this->input->post("submit")){
				$id=$this->input->post("userId");
				$data = array(
					'display_name'=> $this->input->post('fullnames'),
					'email'=> $this->input->post('email'),
					'phone'=> $this->input->post('phone')					
					);
				if ($this->user_accounts_model->update($id, $data))
				{
					// Log the Activity
					log_activity($this->auth->user_id(),"Changed password user details for: User id ".$this->input->post('fullnames'), 'system_utilities');
					Template::set_message('The user details were successfully edited.', 'alert fresh-color alert-success');
					redirect('',true);
				}else{
					Template::set_message('Error Saving!! A problem was encountered editing user details. Please check the values submitted.', 'alert fresh-color alert-danger');
					redirect('',true);
				}
			}else{
			
			$id = $this->input->get("ch2");
			$details = $this->user_accounts_model->get_user_details($id);;
			//$departments = $this->user_accounts_model->get_departments();
			$security_name = $this->security->get_csrf_token_name();
				$security_code = $this->security->get_csrf_hash();
				$url = base_url()."user_management/edit_profile";
				echo <<<eod
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>Edit Your Profile</h3>
						</div>
						<div class="modal-body">
						
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<div class="form-group">
									<label class="col-lg-5 control-label" for="fullnames">Full Names</label>
										<div class="col-lg-3">
											<input id="fullnames" name="fullnames" required="required" type="text" value="$details->display_name" class="form-control" />							
										<input type="hidden" name="userId" value="$id">								
										<input type="hidden" name="$security_name" value="$security_code" > 
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-5 control-label" for="email">email</label>
										<div class="col-lg-3">
											<input id="email" name="email" required="required" type="email" value="$details->email" class="form-control" />							
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-5 control-label" for="phone">Phone Contact</label>
										<div class="col-lg-3">
											<input id="phone" name="phone" required="required" type="number" value="$details->phone" class="form-control" />									
									</div>
								</div>
								
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-pencil-3 white"></span> Save Changes</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
				</div>
eod;
			}
		}
	public function disable_user()
        {
			if($this->input->post("submit")){
				$id=$this->input->post("userId");
				//echo $this->input->post('todo');exit;
				$data = array(
					'banned' => $this->input->post('todo')					
					);
				if ($this->user_accounts_model->update($id, $data))
				{
					// Log the Activity
					log_activity($this->auth->user_id(),"Banned user account for: User id ".$this->input->post('fullnames'), 'system_utilities');
					Template::set_message('The user account status was succesfully changed.', 'alert fresh-color alert-success');					
					redirect('user_management/index',true);
				}else{
					Template::set_message('A problem was encountered changing the user account. Please try again.', 'alert fresh-color alert-danger');					
					redirect('user_management/index',true);
				}
			}else{
				$user_id = $this->input->get("ch2");
				$details = $this->user_accounts_model->as_object()->find($user_id);
				$security_name = $this->security->get_csrf_token_name();
				$security_code = $this->security->get_csrf_hash();
				$url = base_url()."user_management/disable_user";
				if($this->input->get("ch")=="disable"){
					$header="Disable";
					$content="Note: When you disable a user they cannot be able to login or make any transactions. Their previous transactions remain in records though.";
					$form="<input type=\"hidden\" name=\"todo\" value=\"1\">";
				}elseif($this->input->get("ch")=="enable"){
					$header="Activate";
					$content="Note: Activating a user allows them to log in and make transactions. Previous records are still kept for reporting";
					$form="<input type=\"hidden\" name=\"todo\" value=\"0\">";
				}
				echo <<<eod
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>$header User</h3>
						</div>
						<div class="modal-body">
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<h3 align="center">Are you sure you want to <u> $header </u>the user <u> $details->display_name </u></h3>
								<h5 align="center">$content</h5>
								<div class="form-group">							
								<div class="col-lg-3">	
									$form;
									<input type="hidden" name="userId" value="$details->id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							</div>
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-danger"> <span class="icon16 icomoon-icon-users-2 white"></span> $header User</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
				</div>
eod;
			}
		}

    public function filter_allocation()
    {

        if ($this->input->get('ch') == 9) {
            //loads all the facilities
            $data['facilities'] = $this->facilities_model->join("bf_constituencies", "bf_constituencies.id=bf_facilities.sub_county", "left")
                ->select("bf_facilities.*,bf_constituencies.constituency")
                ->where(array('status'=>1))
                ->find_all();
            $this->load->view("user_management/filter_location", $data);

        } elseif ($this->input->get('ch') == 8) {
            //loads all the counties
            $data['counties'] = $this->locations_model->get_counties();
            $this->load->view("user_management/filter_location", $data);
        } else {

        }
    }
}