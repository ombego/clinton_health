
$(".btn_disable_ward").click(function () {
var id=$(this).attr("id");
    $("#ward_to_disable").val(id);
    $("#disable_action").val(0);
    $("#modal_disable_ward").modal('show');

});
$(".btn_enable_ward").click(function () {
var id=$(this).attr("id");
    $("#action").val(1);
    $("#ward_to_enable").val(id);
    $("#modal_enable_ward").modal('show');

});

$(".btn_edit_ward").click(function () {
    var id=$(this).attr("id");
    var cct = $("input[name=ci_csrf_token]").val();

    $.ajax({
        type: "POST",
        data: {id: id,ci_csrf_token:cct},
        url: siteurl + "system_utilities/wards/get_ward",
        success: function (data) {
            var d=JSON.parse(data);
            var ward=d.ward;
            $("input[name=ward_name]").val(ward.ward_name);
            $("input[name=create_action]").val(2);
            $("input[name=id]").val(ward.id);
            $("#description").val(ward.desc);
            $("#btnSave").html("Edit Ward");
            $("input[name=capacity]").val(ward.capacity);


        }
    });


});