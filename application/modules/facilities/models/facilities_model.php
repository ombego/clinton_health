<?php

/**
 * Created by PhpStorm.
 * User: nathan.kamau
 * Date: 27-09-2017
 * Time: 12:00 AM
 */
class Facilities_model extends MY_Model
{
    protected $table_name = 'facilities';
    protected $key = 'id';
    protected $set_created = true;
    protected $log_user = true;
    protected $set_modified = true;
    protected $soft_deletes = false;
    protected $date_format = 'datetime';
    protected $created_field    = 'created_on';
    protected $created_by_field    = 'created_by';
    protected $modified_field    = 'modified_on';
    protected $modified_by_field    = 'modified_by';

    public function get_facilities(){
        return $this->db->query("SELECT bf_facilities.*,constituency FROM bf_facilities
                                    LEFT JOIN bf_constituencies ON bf_constituencies.id=sub_county")->result();
    }

}