<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Locations_model extends MY_Model{
		
		protected $table_name = 'bf_counties';
		protected $key = 'id';
		
		
		public function get_counties()
		{
			return $this->db->query("SELECT * FROM bf_counties order by county_name asc")->result();
		}
        public function get_sub_counties()
        {
            return $this->db->query("SELECT * FROM bf_constituencies order by constituency asc")->result();
        }
		public function get_constituencies($county_id)
		{
			return $this->db->query("SELECT * FROM bf_constituencies WHERE countyid_fk= '".$county_id."' order by constituency asc")->result();
		}
		public function get_county_ward ($constituency_id)
		{
			return $this->db->query("SELECT * FROM bf_county_wards WHERE constituencyid_fk='".$constituency_id."' order by ward_name asc")->result();
		}
		
		
		
	}