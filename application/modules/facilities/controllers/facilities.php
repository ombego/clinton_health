<?php

/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 14-05-2019
 * Time: 11:45 PM
 */
class Facilities extends Front_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->library('users/auth');
        $this->load->helper('form_helper');
        $this->load->model('facilities_model');
        $this->load->model('locations_model');

    }

    public function index()
    {
        $this->auth->restrict('Vision.Facilities.View');
        if ($this->input->post('facility_name')) {
            $facility = array(
                "facility_name" => $_POST['facility_name'],
                "category" => $_POST['category'],
                "county" => $_POST['counties'],
                "sub_county" => $_POST['sub_county'],
                "status" => 1);
            if ($this->facilities_model->insert($facility)) {
                Template::set_message("Successfully Created Facility <b>" . $_POST['facility_name'] . "</b>", "success");
                log_activity($this->auth->user_id(), "Created new facility: " . $_POST['facility_name'], 'Facilities');
            } else {
                Template::set_message("Error while creating Facility :<b>" . $_POST['facility_name'] . "</b>", "danger");

            }
        }
        Template::set('facilities', $this->facilities_model->join("bf_constituencies", "bf_constituencies.id=bf_facilities.sub_county", "left")
                                    ->select("bf_facilities.*,bf_constituencies.constituency")
                                    ->find_all());
        Template::set('counties', $this->locations_model->get_counties());
        Template::set('sub_counties', $this->locations_model->get_sub_counties());
        //add datatable js
        Assets::add_module_js('system_utilities', 'icheck.min');
        Assets::add_module_js('system_utilities', 'jquery.mCustomScrollbar.min');
        Assets::add_module_js('system_utilities', 'jquery.dataTables.min');
        Assets::add_module_js('system_utilities', 'facilities_custom');
        Template::set_theme('default');
        Template::set('page_title', 'Facilities');
        Template::render('');
    }

    public function filter_location()
    {

        //$this->load->model('locations_model');
        if ($this->input->get('ch2') == 1) {
            //loads all the counties
            $data['counties'] = $this->locations_model->get_counties();
            $this->load->view("facilities/filter_location", $data);

        } elseif ($this->input->get('ch2') == 2) {
            //loading constituencies
            $county_id = $this->input->get('ch');
            $data["selected_constituency"] = 0;
            $data["selected_ward"] = null;
            if ($this->input->get('ch3')) {
                //we are editing
                $data["selected_constituency"] = $this->input->get('ch3');
                $data["selected_ward"] = $this->input->get('ch4');
            }
            $data['constituencies'] = $this->locations_model->get_constituencies($county_id);
            $this->load->view("facilities/filter_location", $data);
        } elseif ($this->input->get('ch2') == 3) {
            //loading constituencies
            $constituency_id = $this->input->get('ch');
            ///for editing purposes
            $data["selected_constituency"] = 0;

            $data["selected_ward"] = null;
            if ($this->input->get('ch3')) {
                //we are editing
                $data["selected_ward"] = $this->input->get('ch3');
            }
            $data['county_ward'] = $this->locations_model->get_county_ward($constituency_id);
            $this->load->view("facilities/filter_location", $data);
        }
    }

    public function disable_facility()
    {
        if ($this->input->post("submit")) {
            $id = $this->input->post("facility_id");
            //echo $this->input->post('todo');exit;
            $data = array('status' => $this->input->post('todo'));
            if ($this->facilities_model->update($id, $data)) {
                // Log the Activity
                log_activity($this->auth->user_id(), "Disabled Facility, Facility id " . $this->input->post('facility_id'), 'facilities');
                Template::set_message('The facility was succesfully disabled.', 'alert fresh-color alert-success');
                redirect('facilities/index', true);
            } else {
                Template::set_message('A problem was encountered disabling the facility. Please try again.', 'alert fresh-color alert-danger');
                redirect('facilities/index', true);
            }
        } else {

            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url() . "facilities/disable_facility";
            if ($this->input->get("ch2") == "disable") {
                $header = "Disable";
                $content = "Note: When you disable a facility users cannot make new orders to the facility. Previous transactions remain in records though.";
                $form = "<input type=\"hidden\" name=\"todo\" value=\"0\">";
            } elseif ($this->input->get("ch2") == "enable") {
                $header = "Activate";
                $content = "Note: Activating a user allows users to continue making orders through the facility. Previous records are still kept for reporting";
                $form = "<input type=\"hidden\" name=\"todo\" value=\"1\">";
            }
            echo <<<eod
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>$header facility</h3>
						</div>
						<div class="modal-body">
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<h3 align="center">Are you sure you want to <u> $header </u>the facility <u> $details->facility_name </u></h3>
								<h5 align="center">$content</h5>
								<div class="form-group">							
								<div class="col-lg-3">	
									$form;
									<input type="hidden" name="facility_id" value="$details->id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							</div>
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-danger"> <span class="icon16 icomoon-icon-users-2 white"></span> $header facility</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
				</div>
eod;
        }
    }
}