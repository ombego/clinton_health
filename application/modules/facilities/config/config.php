<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'To manage all facilities configurations',
    'name'          => 'Facilities',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);