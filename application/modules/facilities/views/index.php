<div class="page-title">
    <h2><span class="fa fa-arrow-circle-o-left"></span> <?php echo $page_title; ?></h2>

</div>

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Manage Facilities </h3>
                </div>

                <div class="panel-body">
                    <div class="block">
                        <div class="col-md-4 col-lg-4 col-md-4">
                            <h4>Create New Facility</h4>
                            <form role="form" method="post" id="facility" action="<?php echo current_url() ?>" >
                                <div class="form-group">
                                    <label>Facility Name</label>
                                    <input type="text" class="form-control" required="required" name="facility_name" placeholder="Enter Facility Name.."/>
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash() ?>">
                                </div>
                                <div class="form-group">
                                    <label class="">Facility Category</label>
                                    <select class="form-control" name="category" required>
                                        <option value="" selected disabled>Select Category</option>
                                        <option value="D-CDRR" >Central Facility </option>
                                        <option value="F-CDRR" >Stand-alone Facility</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>County</label>
                                    <select class="form-control" data-placeholder="Choose a County..." id="counties" name="counties" required="required" onChange="htmlData2('<?php echo base_url();?>facilities/filter_location', 'ch='+this.value+'&ch2=2')">
                                        <option value="">Select County</option>

                                        <?php
                                            foreach ($counties as $rows){
                                                echo "<option value=\"".$rows->county_id."\" >".$rows->county_name." </option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group" id="txtResult2">
                                    <label>Sub-County</label>
                                    <select class="form-control" data-placeholder="Select Sub County..." id="sub_counties" name="sub_counties" required="required">
                                        <option value="" selected disabled>Select Sub-County</option>

                                        <?php
                                        foreach ($sub_counties as $rows){
                                            echo "<option value=\"".$rows->id."\" >".$rows->constituency." </option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear</button>
                                    <button class="btn btn-primary pull-right" id="btnSave" type="submit">Create Facility</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <h4>Facilities</h4>
                            <div class="panel panel-default tabs">
                                <ul class="nav nav-tabs  ">
                                    <li class="active"><a href="#active_wards" data-toggle="tab">Active Facilities</a></li>
                                    <li><a href="#inative_wards" data-toggle="tab">Inactive Facilities</a></li>

                                </ul>
                                <div class="panel-body tab-content">
                                    <div class="tab-pane active" id="active_wards">
                                        <table class="table ">
                                            <thead>
                                            <tr>
                                                <th>Facility</th>
                                                <th>Category</th>
                                                <th>Sub County</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (isset($facilities) && is_array($facilities)):
                                                foreach ($facilities as $facility): if ($facility->status == 1): ?>
                                                    <tr>
                                                        <td><?php echo $facility->facility_name; ?></td>
                                                        <td><?php echo $facility->category; ?></td>
                                                        <td><?php echo $facility->constituency ; ?> </td>
                                                        <td><span class="text-success">active</span></td>
                                                        <td>
                                                            <?php if($facility->status==1): ?>
                                                                <a href="#txtResult" data-toggle="modal" title="Edit Facility" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url(); ?>facilities/edit_facility', 'ch=<?php echo $facility->id; ?>')"><i class="fa fa-edit"></i></a>
                                                                <a href="#txtResult" data-toggle="modal" title="Deactivate Facility" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url(); ?>facilities/disable_facility', 'ch=<?php echo $facility->id; ?>&ch2=disable')"><i class="fa fa-lock"></i></a>
                                                            <?php else: ?>
                                                                <a href="#txtResult" data-toggle="modal" title="Activate Facility" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url(); ?>facilities/disable_facility', 'ch=<?php echo $facility->id; ?>&ch2=enable')"><i class="fa fa-unlock-alt"></i></a>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                            <?php
                                                endif; endforeach; endif;
                                            ?>

                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="tab-pane" id="inative_wards">
                                        <table class="table ">
                                            <thead>
                                            <tr>
                                                <th>Facility</th>
                                                <th>Category</th>
                                                <th>Sub County</th>
                                                <th>Status</th>
                                                <th>Date Created</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if (isset($facilities) && is_array($facilities)):
                                                foreach ($facilities as $facility): if ($facility->status == 0): ?>
                                                <tr>
                                                    <td><?php echo $facility->facility_name; ?></td>
                                                    <td><?php echo $facility->category; ?></td>
                                                    <td><?php echo $facility->constituency ; ?> </td>
                                                    <td><span class="label label-danger">in-active</span></td>
                                                    <td><?php echo date('d-mm-Y',strtotime($facility->created_on)); ?></td>
                                                    <td>
                                                        <a href="#txtResult" data-toggle="modal" title="Activate Facility" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url(); ?>facilities/disable_facility', 'ch=<?php echo $facility->id; ?>&ch2=enable')"><i class="fa fa-unlock-alt"></i></a>
                                                    </td>
                                                </tr>
                                            <?php
                                                endif; endforeach;endif;
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<div id="txtResult" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true"> </div>
