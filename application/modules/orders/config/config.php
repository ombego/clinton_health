<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'Creating of orders and tracking user orders',
    'name'          => 'Orders',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);