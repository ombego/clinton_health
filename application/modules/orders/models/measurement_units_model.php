<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Measurement_units_model extends MY_Model {
	protected $table_name = 'stores_measurement_units';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';
    
	public function get_queue_patients()
	{
		$today = date('Y-m-d');
		return $this->db->query("SELECT DISTINCT(patient_id),bf_emr_out_patients.* FROM bf_emr_queue 
									LEFT JOIN bf_emr_out_patients ON bf_emr_out_patients.id=patient_id
									WHERE date(time_in)='".$today."' and stage_id=2 and bf_emr_queue.status=0
									ORDER BY time_in asc")->result();
		
	}
	
}