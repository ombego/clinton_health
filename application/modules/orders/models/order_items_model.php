<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_items_model extends MY_Model {
	protected $table_name = 'order_items';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_ordered_items($id)
    {
        return $this->db->query("SELECT bf_order_items.* FROM bf_order_items
									WHERE order_id='".$id."'")->result();
    }
    public function get_user_orders($user_id)
    {
        return $this->db->query("SELECT bf_orders.*,display_name FROM bf_orders 
									LEFT JOIN bf_users ON bf_users.id = order_by
									WHERE order_by='".$user_id."' ORDER BY id DESC LIMIT 20")->result();
    }
    public function get_store_order_item($id)
    {
        return $this->db->query("SELECT bf_order_items.*,name FROM bf_order_items
                                    LEFT JOIN bf_inventory ON bf_inventory.id=product_id
									WHERE bf_order_items.id='".$id."'")->row();
    }
    public function delete_item($id)
    {
        $this->db->query("DELETE FROM bf_order_items WHERE id='".$id."'");
    }
}