<!-- PAGE CONTENT WRAPPER -->
                
				<div class="content-frame">
                   <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-trello"></span> My Orders</small></h2>
                        </div>              
                    </div>
                   <!-- END CONTENT FRAME TOP -->
				   <!-- START CONTENT FRAME LEFT -->
                    <div class="content-frame-left">
                        <div class="block">
                            <a href="<?php echo base_url()."orders/create_order"; ?>" class="btn btn-warning btn-block btn-lg"><span class="fa fa-shopping-cart"></span> Create New Order</a>
                        </div>
                        <div class="block">
                            <div class="list-group border-bottom">
                                <a href="#" data-toggle="modal" data-target=".search_orders" class="list-group-item active"> My Orders

                                </a>
								<?php foreach($orders as $order):?>
									<a href="<?php echo base_url()."orders/index/".$order->id;?>" class="list-group-item">
										<?php if($order->status==2): ?>
                                            <span class="fa fa-check" title="Order Approved">
                                        <?php elseif($order->status==1): ?>
                                            <span class="fa fa-pencil-square-o" title="Order not submitted">
                                        <?php elseif($order->status==-1): ?>
                                                <span class="fa fa-spinner" title="Awaiting Approval">
                                        <?php elseif($order->status==3): ?>
                                            <span class="fa fa-thumbs-up" title="Order Received">
                                        <?php elseif($order->status==4): ?>
                                            <span class="fa fa-shopping-cart" title="Order Under Procurement">
                                        <?php else: ?>
                                            <span class="fa fa-exclamation-triangle" title="Order Cancelled">
                                        <?php endif; ?>
											&nbsp;<?php echo "Order - ".$order->id; ?>
										</span> 

										<span class="badge badge-default"><?php echo date('d-M-Y', strtotime($order->order_date)); ?></span>
									</a>
                                <?php endforeach; ?>
								
                            </div>                        
                        </div>
                        
                    </div>
				   <!-- END CONTENT FRAME LEFT -->
					<!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body">
                       <?php if(ISSET($order_details)): ?> 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 <span class="h3 text-info">Order <?php echo $order_details->id; ?></span>
                                <div class="pull-right" style="width: 159px;">
                                    <div class="input-group">
                                       <?php if($order_details->status==1 or $order_details->status==-1): ?>
										<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target=".add_items"> <span class="fa fa-plus"></span> Add Item to Order</button>
									   <?php elseif($order_details->status==0): ?>
										<span class="h3 text-danger"><span class="fa fa-exclamation-triangle"></span>Order Cancelled</span>
									   <?php elseif($order_details->status==2): ?>
										<span class="h3 text-info"><span class="fa fa-check"></span>Order Approved</span>
                                       <?php elseif($order_details->status==3): ?>
                                           <span class="h3 text-success"><span class="fa fa-thumbs-up"></span>Dispensed</span>
                                       <?php elseif($order_details->status==5): ?>
                                           <span class="h3 text-success"><span class="fa fa-thumbs-up"></span>Received</span>
                                       <?php elseif($order_details->status==4): ?>
                                           <span class="h3 text-warning"><span class="fa fa-shopping-cart"></span>Awaiting Procurement</span>
									   <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body mail">
							<?php if($received_items): foreach($received_items as $row):?>
                                <div class="mail-item mail-info">                                    
									<div class="mail-checkbox">										
										<?php if($order_details->status==1 or $order_details->status==-1): ?><a href="#txtResult" data-toggle="modal" class="text-info" onclick="htmlData('<?php echo base_url();?>orders/edit_store_order_item', 'ch2=<?php echo $row->id?>')"><span class="fa fa-edit"></span></a>	<?php endif; ?>
                                    </div>
                                    <div class="mail-user"><?php echo $row->name; ?></div>
                                    <a href="pages-mailbox-message.html" class="mail-text"><span class="text-muted">Order Qty </span>| <?php echo number_format($row->order_qty); echo ($row->comments)?" <br> <mute><small>".$row->comments."</small></mute>":"";?> </a>
                                    <div class="mail-date"> <?php echo "<br>Received: 0 : -"; ?> </div>
                                    <div class="mail-attachments">
                                        <span class="fa fa-inbox"></span> Qty Approved: 0
                                    </div>
                                </div>
							<?php endforeach; endif; ?>								 
                                
                            </div>
                            <div class="panel-footer">
                                <?php if($order_details->status==1 OR $order_details->status==-1): ?>
                                    <a href="<?php echo base_url()."orders/delete_store_order/".$order_details->id;?>"  class="btn btn-default btn-lg  pull-left" title="Delete Order">Delete</a>
                                <?php endif; ?>
                                <ul class="pagination pagination-sm pull-right">                                    
                                   <?php if($order_details->status==1): ?>
                                       <li class="active">
                                           <a href="<?php echo base_url();?>orders/submit_order/<?php echo $order_details->id; ?>" class="btn btn-warning btn-lg">Submit order</a>
                                       </li>
                                   <?php elseif($order_details->status==-1): ?>
                                       <li class="active"><a href="<?php echo base_url();?>orders/orders/index ?>" class="btn btn-warning btn-lg">Save Changes</a></li>
                                   <?php endif;?>
                                </ul>
                            </div>                            
                        </div>
                      <?php endif; ?>  
                    </div>
                    <!-- END CONTENT FRAME BODY -->
                </div>
                <div id="txtResult" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true"> </div>
				
				<!-- Modal for receiving item --> 
							
								<div class="modal fade add_items" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
										<form method="post" id="new_prescription" name="new_prescription" class="form-horizontal" role="form" action="<?php echo base_url(); ?>orders/add_order_item" accept-charset="utf-8" autocomplete="off">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Add Order Item</h4>
                                            </div>
                                            <div class="modal-body">                                                
                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
												<input type="hidden" value="<?php echo $order_details->id;?>" name="order_id">
												<input type="hidden" value="1" name="payment_method">
												<div class="row">
													<div class="block">
														<div class="form-group">
															<label class="col-md-3 control-label">Search Item</label>
															<div class="col-md-7">
																<div class="input-group">
																	<span class="input-group-addon"><span class="fa fa-search"></span></span>
																	<input type="text" class="form-control" id="box1Filter" name="search" placeholder="Search.." required="required" onkeypress="htmlData2('<?php echo base_url();?>orders/get_store_search_items', 'ch='+this.value)">
																</div>
															</div>
														</div>												
													</div>
												</div>
												<div id="txtResult2"> </div>
												
                                            
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <input type="submit" name="submit"  class="btn btn-warning" value="Add To Order">
                                            </div>
											</div>
										</form>
                                        </div>
                                    </div>
                                </div>

<!-- Modal for searching orders -->

<div class="modal fade search_orders" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Search Orders</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-3">
                        <form class="form-horizontal" name="search_form" id="search_form">
                            <!-- START ACCORDION -->
                            <div class="panel-group accordion">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a href="#accOneColOne">
                                                Search by Items Ordered
                                            </a>
                                        </h5>
                                    </div>
                                    <div class="panel-body panel-body-open" id="accOneColOne">
                                        <div class="block">
                                            <div class="form-group">
                                                <label>Item Name</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                                    <input type="text" class="form-control" id="box1Filter" name="search" placeholder="Enter Item Name.." required="required" onkeypress="htmlData3('<?php echo base_url();?>stores/search/get_store_search_items', 'ch='+this.value)">
                                                </div>
                                            </div>
                                            <div class="form-group" id="txtResult3">
                                                <select id="box1View" name="items[]" class="form-control" required="required" size="6" multiple="multiple" ></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a href="#accOneColTwo">
                                                Search by Time Period
                                            </a>
                                        </h5>
                                    </div>

                                    <div class="panel-body" id="accOneColTwo">
                                        <div class="block">
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    <input type="date" class="form-control" id="start_date" name="start_date" placeholder="Start Date" required="required" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>End Date</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    <input type="date" class="form-control" id="end_date" name="end_date" placeholder="End Date" required="required" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-danger pull-right" onclick="htmlData4('<?php echo base_url();?>stores/search/show_search_results', 'date1='+search_form.start_date.value+'&date2='+search_form.end_date.value)"><span class="fa fa-search"></span>Go</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- todo: Add search orders by users
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColThree">
                                                Ordered By
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="accOneColThree">

                                    </div>
                                </div> -->
                            </div>
                            <!-- END ACCORDION -->

                        </form>
                        <br><hr>
                    </div>
                    <div class="col-lg-9" id="txtResult4">
                        <table class="table table-responsive dataTable">
                            <thead>
                                <tr>
                                    <th>Order #</th>
                                    <th>Order Date</th>
                                    <th>Order By</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 </div>
            </div>
        </div>

</div>