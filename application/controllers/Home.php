<?php defined('BASEPATH') || exit('No direct script access allowed');
//require APPPATH . 'libraries/REST_Controller.php';


class Home extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library('users/auth');
        $this->auth->restrict();
		$this->load->helper('application');
		$this->load->library('Template');
		$this->load->library('Assets');
		$this->lang->load('application');
		$this->load->library('events');
		//$this->load->library('REST_Controller');

        $this->load->library('installer_lib');
        if (! $this->installer_lib->is_installed()) {
            $ci =& get_instance();
            $ci->hooks->enabled = false;
            redirect('install');
        }

        $this->load->model('dashboard_model');
        // Make the requested page var available, since
        // we're not extending from a Bonfire controller
        // and it's not done for us.
        $this->requested_page = isset($_SESSION['requested_page']) ? $_SESSION['requested_page'] : null;
	}

	//--------------------------------------------------------------------

	/**
	 * Displays the homepage of the Bonfire app
	 *
	 * @return void
	 */
	public function index(){

        $this->load->library('users/auth');
        $this->auth->restrict();
        $this->set_current_user();
        if($this->current_user->role_id==9){
            $facility_id  = $county_id = $this->current_user->allocation;
            Template::set('orders', $this->dashboard_model->get_facility_orders($facility_id));
        }elseif($this->current_user->role_id==8){
            $county_id = $this->current_user->allocation;
            Template::set('orders', $this->dashboard_model->get_county_orders($county_id));
        }else{
            Template::set('orders', $this->dashboard_model->get_orders());
        }

        Template::set_theme('default');
        Template::set('page_title', 'Dashboard');
		Template::render();
	}//end index()
    public function order_details(){
        $id = $this->input->get("ch");
        $data['order_items'] = $this->dashboard_model->get_order_items($id);
        $this->load->view("home/order_details", $data);
    }
    public function get_data_api($id)
    {
        $data = $this->dashboard_model->get_order_items($id);
        $this->response($data, REST_Controller::HTTP_OK);

    }
	//--------------------------------------------------------------------

	/**
	 * If the Auth lib is loaded, it will set the current user, since users
	 * will never be needed if the Auth library is not loaded. By not requiring
	 * this to be executed and loaded for every command, we can speed up calls
	 * that don't need users at all, or rely on a different type of auth, like
	 * an API or cronjob.
	 *
	 * Copied from Base_Controller
	 */
	protected function set_current_user()
	{
        if (class_exists('Auth')) {
			// Load our current logged in user for convenience
            if ($this->auth->is_logged_in()) {
				$this->current_user = clone $this->auth->user();

				$this->current_user->user_img = gravatar_link($this->current_user->email, 22, $this->current_user->email, "{$this->current_user->email} Profile");

				// if the user has a language setting then use it
                if (isset($this->current_user->language)) {
					$this->config->set_item('language', $this->current_user->language);
				}
            } else {
				$this->current_user = null;
			}

			// Make the current user available in the views
            if (! class_exists('Template')) {
				$this->load->library('Template');
			}
			Template::set('current_user', $this->current_user);
		}
	}
}
/* end ./application/controllers/home.php */
