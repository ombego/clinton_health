<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Welcome to Ordering System</title>
        <base target="_blank">
        <link rel="stylesheet" type="text/css" href="public/assets/css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="public/assets/css/bootstrap-responsive.min.css" media="screen" />
        <style>
            body {
                font-family:sans-serif;
            }
            #intro {
                width:700px;
                margin-left:-390px;
                position:fixed;
                left:50%;
                top:60px;
                padding:10px 30px;
            }
            h1 {
                text-align:center;
            }
            .continue {
                padding:10px 0;
                text-align:center;
            }
        </style>
    </head>
    <body>
        <div id="intro" class="well">
            <h1>Welcome</h1>
            <p>This is a drug ordering and allocation project developed by  <strong>Edwin Gisore Ombego</strong> as a technical assignment in pursuant to a Systems Developer position at Clinton Health Access Initiative (CHAI).</p>

            <p>Features:</p>
            <ol>
                <li>Developed on CI-Bonfire a CodeIgniter HMVC Framework </li>
                <li>Role-Based Access Control system</li>
                <li>Mobile Friendly Bootsrap User Interface</li>
                <li>Facility Management</li>
                <li>User Management</li>
                <li>Order Creation and Tracking</li>
                <li>Inventory management</li>
            </ol>

            <p>

                <em>Please Note:</em>
                In the project directory there is a database file named <em>database.sql</em> located in the directory named 'Database'.
                <br> The Following User Accounts have already been created for the testing
                <ol>
                    <li>Username: county ~ Password: password</li>
                    <li>Username: subcounty ~ Password: password</li>
                    <li>Username: national ~ Password: password</li>
                </ol>
                </p>

            <p><em>"If you need a walkthrough you can get intouch with me on
                    <ul> <li>Skype: ombego</li>
                        <li>email :<a href="mailto:ombego@gmail.com/"> ombego@gmail.com</a></li>
                        <li>Phone : 0724858611/0738055312</li>
                    </ul>
                    Thank you for this opportunity, Hoping to get an opportunity to join your team."</em> ~ Edwin Ombego</p>
            <div class="continue">
                <a class="btn btn-primary" target="_self" href="public">Continue &raquo;</a>
            </div>
        </div>
        <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="public/assets/js/jquery-1.7.2.min.js"><\/script>');
        </script>
        <!-- This would be a good place to use a CDN version of jQueryUI if needed -->
        <script type="text/javascript" src="public/assets/js/bootstrap.min.js" ></script>
    </body>
</html>