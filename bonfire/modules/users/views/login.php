<div class="login-container">

    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        <div class="login-body">
            <div class="login-title"><strong>Welcome</strong>, Please login</div>
            <?php echo Template::message();
                     if (validation_errors()) {echo validation_errors();}

        ?>

    <div id="loginform">
        <?php echo form_open(LOGIN_URL, array('autocomplete' => 'off','class'=>'form-horizontal',"id"=>"loginForm","role"=>"form")); ?>
        <div class="form-group">
            <div class="col-md-12">
                <input type="text" class="form-control" name="login" id="login_value" value="<?php echo set_value('login'); ?>" tabindex="1" placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>"/>
                <input type="hidden" name="log-me-in" value="<?php e(lang('us_let_me_in')); ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">

                <input type="password" class="form-control" name="password" id="password" value="" tabindex="2" placeholder="<?php echo lang('bf_password'); ?><?php echo iif( form_error('password') , 'error') ;?>"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6">
                <a href="<?php echo base_url(); ?>forgot_password" class="btn btn-link btn-block">Forgot your password?</a>
            </div>
            <div class="col-md-6">
                <input type="submit" class="btn btn-info btn-block" value="Log In">
            </div>
        </div>

    <?php echo form_close(); ?>
    </div>
    <?php // show for Email Activation (1) only
    if ($this->settings_lib->item('auth.user_activation_method') == 1) : ?>
        <!-- Activation Block -->
        <p style="text-align: left" class="well">
            <?php echo lang('bf_login_activate_title'); ?><br />
            <?php
            $activate_str = str_replace('[ACCOUNT_ACTIVATE_URL]',anchor('/activate', lang('bf_activate')),lang('bf_login_activate_email'));
            $activate_str = str_replace('[ACTIVATE_RESEND_URL]',anchor('/resend_activation', lang('bf_activate_resend')),$activate_str);
            echo $activate_str; ?>
        </p>
    <?php endif; ?>



        </div>
        <div class="login-footer">
            <div class="pull-left">
                &copy; 2019 xyzzy
            </div>
            <div class="pull-right">
                <a href="#">About</a> |
                <a href="#">Privacy</a> |
                <a href="#">Contact Us</a>
            </div>
        </div>
        <div class="margin text-center">
            <span class="white"><a href="#" valign="bottom">Edwin G. Ombego</span>
        </div>
    </div>

</div>
