<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="index.html">Orders System</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="<?php echo Template::theme_url('images/users/avatar.jpg');?>" alt="<?php echo $current_user->display_name; ?>"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="<?php echo Template::theme_url('images/users/avatar.jpg');?>" alt="<?php echo $current_user->display_name; ?>"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php echo $current_user->display_name; ?></div>
                    <div class="profile-data-title"><?php echo $current_user->email; ?></div>
                </div>
                <div class="profile-controls">
                    <a href="#" title="My Profile" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="#l" title="Edit Profile" class="profile-control-right"><span class="fa fa-edit"></span></a>
                </div>
            </div>
        </li>
        <li class="xn-title">Navigation</li>
        <li class="active">
            <a href="<?php echo base_url(); ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
        </li>
        <?php if(has_permission('Vision.Orders.Manage')):?>
        <li class="xn-openable">
            <a href="#"><span class="fa fa-stack-overflow"></span> <span class="xn-text">Orders</span></a>
            <ul>
                <li><a href="<?php echo base_url();?>orders"><span class="fa fa-shopping-cart"></span> Create Order</a></li>
            </ul>
        </li>
        <?php endif; ?>
        <li class="xn-title">Settings</li>
        <li class="xn-openable">
            <a href="#"><span class="fa fa-hospital-o"></span> <span class="xn-text">Facilities</span></a>
            <ul>
                <li><a href="<?php echo base_url();?>facilities"><span class="fa fa-building-o"></span> Manage Facilities</a></li>

            </ul>
        </li>
        <li class="xn-openable">
            <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Users</span></a>
            <ul>
                <li><a href="<?php echo base_url(); ?>user_management"><span class="fa fa-user-md"></span> Manage Users</a></li>

            </ul>
        </li>

    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->
