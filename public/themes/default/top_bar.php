	<!-- PAGE CONTENT -->
            <div class="page-content">	
		<!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="<?php echo base_url();?>logout" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
                    </li> 
                    <!-- END SIGN OUT -->
                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
				<?php if($this->uri->segment(1) == "dashboard"): ?>
					<li><a href="#">Dashboard</a></li>
				<?php else: ?>
                    <li><a href="javascript:history.go(-1)">Dashboard</a></li>                    
                    <li class="active"><?php echo $page_title; ?></li>
				<?php endif; ?>
                </ul>
                <!-- END BREADCRUMB --> 
				<?php echo Template::message(); ?>
