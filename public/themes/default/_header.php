<?php

Assets::add_js(array('js/lib/jquery.min.js','js/validator/validator.js') );

// Core stylesheets do not remove -->
Assets::add_css(array('theme-default.css') );


?>


<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>CHAI | Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?php echo Template::theme_url('images/favicon.ico');?>" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <?php echo Assets::css(); ?>
    <!-- EOF CSS INCLUDE -->
    <!--        define my javascript global variables - Nathan-->
    <script type="text/javascript">
        var siteurl="<?php echo base_url()?>";
    </script>
</head>
<body>
