
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->


<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out?</p>
                <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="<?php echo base_url(); ?>logout" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->

<!-- Edit user profile Message box -->
<div class="message-box message-box-primary animated fadeIn" id="message-box-profile">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-user"></span>Edit Profile
                <strong><?php echo $current_user->display_name; ?></strong></div>
            <form method="POST" action="<?php echo base_url(); ?>system_utilities/users/edit_profile" name="admission" id="admission"
                  class="form-horizontal">
                <div class="mb-content">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash() ?>">
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="fullnames">Full Names</label>
                        <div class="col-lg-5">
                            <input id="fullnames" name="fullnames" required="required" type="text" value="<?php echo $current_user->display_name; ?>" class="form-control" />
                            <input type="hidden" name="userId" value="<?php echo $current_user->id; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="email">email</label>
                        <div class="col-lg-5">
                            <input id="email" name="email" required="required" type="email" value="<?php echo $current_user->email; ?>" class="form-control" />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="phone">Phone Contact</label>
                        <div class="col-lg-5">
                            <input id="phone" name="phone" required="required" type="number" value="<?php echo $current_user->phone; ?>" class="form-control" />
                        </div>
                    </div>

                </div>
                <div class="mb-footer">
                    <button class="btn btn-default btn-lg pull-left mb-control-close">Close</button>
                    <input type="submit" class="btn btn-warning btn-lg pull-right" name="submit" value="Update Profile">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit user password Message box -->
<div class="message-box message-box-primary animated fadeIn" id="message-box-password-change">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-key"></span>Edit Your Password
                <strong><?php echo $current_user->display_name; ?></strong></div>
            <form method="POST" action="<?php echo base_url(); ?>system_utilities/users/edit_password" name="admission" id="admission"
                  class="form-horizontal">
                <div class="mb-content">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash() ?>">
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="password">New Password</label>
                        <div class="col-lg-5">
                            <input id="password" name="password" required="required" type="password"  class="form-control" />
                            <input type="hidden" name="userId" value="<?php echo $current_user->id; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="repeat_password">Confirm Password</label>
                        <div class="col-lg-5">
                            <input id="repeat_password" name="repeat_password" required="required" type="password"  class="form-control" />

                        </div>
                    </div>

                </div>
                <div class="mb-footer">
                    <button class="btn btn-default btn-lg pull-left mb-control-close">Close</button>
                    <input type="submit" class="btn btn-danger btn-lg pull-right" name="submit" value="Change Password">
                </div>
            </form>
        </div>
    </div>
</div>


<!-- START SCRIPTS -->

<script type="text/javascript" src="<?php echo Template::theme_url('js/plugins/jquery/jquery-ui.min.js');?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/plugins/bootstrap/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/plugins/bootstrap/bootstrap-select.js');?>"></script>
<?php echo Assets::js(); ?>
<!-- START PRELOADS -->
<audio id="audio-alert" src="<?php echo Template::theme_url('audio/alert.mp3');?>" preload="auto"></audio>
<audio id="audio-fail" src="<?php echo Template::theme_url('audio/fail.mp3');?>" preload="auto"></audio>
<!-- END PRELOADS -->


<!-- END TEMPLATE -->
<!-- END SCRIPTS -->
</body>
</html>
