<?php

Assets::add_js(array('plugins/jquery-validation/jquery.validate.js','plugins/icheck/icheck.min.js','plugins/summernote/summernote.js','plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js','plugins/scrolltotop/scrolltopcontrol.js','plugins/bootstrap/bootstrap-datepicker.js','plugins/owl/owl.carousel.min.js','plugins/moment.min.js','plugins/daterangepicker/daterangepicker.js','plugins.js','actions.js','ajax_req.js','validator/validator.js'));

Assets::add_css(array('theme-default.css'));


?>

<!--
Author: Edwin Ombego
Author URL: http://ke.linkedin.com/pub/eddie-ombegoh/29/293/522
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>CHAI | <?php echo(ISSET($page_title))?$page_title:'Drug Orders';?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?php echo Template::theme_url('images/favicon.ico');?>" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <?php echo Assets::css(); ?>
    <!-- EOF CSS INCLUDE -->
</head>
<body>
<script type="text/javascript" src="<?php echo Template::theme_url('js/plugins/jquery/jquery.min.js');?>"></script>
<!-- START PAGE CONTAINER -->
<div class="page-container">